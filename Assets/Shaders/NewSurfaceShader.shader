﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/NewSurfaceShader" {
	// Properties section makes a bunch of properties available in the editor for configuration. Similar to MonoBehaviour public fields
	// NOTE: setting variables here is NOT enough to actually use them. They still need to be defined in the shader body.
	Properties {
		// defines a property, accessible in the Unity Editor
		// format is _VariableName ("Name string", DataType) = "Initialiser" {}
		// Initialisers can be e.g. "white" to set the default texture values to represent the colour white, etc
		// another example is _MyInt ("My Integer", Int) = 2
		_MainTex ("Texture", 2D) = "white" {}
		// Note that Cg/HLSL language has the following common types
		// int, float, half4, float4, half2, float2
		// And while Unity will work if you use Vector, it will just be treated as a float2, and the last two values will be ignored
	}
	SubShader {
		// No culling or depth
		Cull Off Zwrite Off ZTest Always

		// Tags are used to tell Unity certain properties of our shader. For instance, the render order and type
		// RENDER ORDER:
		// This is, simply, the order in which verts are drawn. By default, verts further from the camera are drawn first.
		// However, with transparency, this may need manual changes. This is handled via tagging. Different pre-defined tags correspond to different positions in the Queue
		// Background = 1000:
		// Geometry = 2000: The "default", used for most shapes etc
		// Transparent = 3000: Used for transparent/semitransparent materials
		// Overlay = 4000: used for camera effects, lens flares, etc
		
		// This however ignores the ZTest, which will determine the depth of each object for each pixel, and discard any that would be drawn over, regardless
		// of the order in which they are drawn to the screen.
		Tags {
			"Queue" = "Geometry"
			"RenderType" = "Opaque"
		}

		// The Cg section of vertex and fragment shaders need to be enclosed in a Pass section.
		// This is not the case for simple surface shaders, which will work with or without it.
		Pass {
			// all the actual Cg code is contained in this section (CGPROGAM)
			// Performance here is critical as loosely speaking, this is all run once per pixel
			// in fact, there is a limit on the number of operations that can be performed in a shader (due to GPU architecture limits)
			CGPROGRAM
			// Vertex and Fragment shaders
			// The geomery of the given model is passed through a function called vert which can alter its vertices.
			// Then, individual triangles are passed through the frag function, which runs calculation on each pixel.
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			// As stated above, this is run first, and the output passed into frag.
			v2f vert (appdata v)
			{
				v2f o;
				// Transforms a point from object space to the camera's clip space.
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			// Even though _MainTex is set in the Properties section, we still define it here for use by the shader program
			// fun fact: the type here does NOT need to match that of the variable declared above, but the name MUST be declared above.
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv + float2(0, sin(i.vertex.x/50 + _Time[0]*2)/10));
				return col; 
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
